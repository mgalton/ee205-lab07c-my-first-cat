//////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
//
// @file    hello3.cpp
// @version 1.0 - Initial implementation
//
// Build and test 3 Hello World Programs
//
// @author  Mariko Galton <mgalton@hawaii.edu>
// @@date   3_Mar_2022
//
// @see     https://www.gnu.org/software/make/manual/make.html
//
////////////////////////////////////////////////////////////////

#include <iostream>

 class Cat {
   public:
      void sayHello() {
         std::cout << "Meow!" <<std::endl;
      }
   };

int main() {

      Cat myCat;
      myCat.sayHello();
   }
